module Data.Either

%default total

public export
data Either a b = Left a | Right b
